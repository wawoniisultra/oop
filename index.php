<?php

    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');


    $sheep = new animal('shaun');
    echo "Name = " . $sheep->name . "<br>";
    echo "Legs = " . $sheep->legs . "<br>";
    echo "cold blooded = " . $sheep->cold_blooded . "<br> <br>";

    $kodok = new Frog("buduk");
    echo "Name = " . $kodok->name . "<br>";
    echo "Legs = " . $kodok->legs . "<br>";
    echo "cold blooded = " . $kodok->cold_blooded . "<br>";
    $kodok->jump("hop hop"); 
    echo "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Name = " . $sungokong->name . "<br>";
    echo "Legs = " . $sungokong->legs . "<br>";
    echo "cold blooded = " . $sungokong->cold_blooded . "<br>";
    $sungokong->yell("Auooo");



?>